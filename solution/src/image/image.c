#include "image.h"
#include <stdlib.h>

struct image init_image(size_t width, size_t height) {
  struct pixel *data = malloc(sizeof(struct pixel) * width * height);
  return (struct image) {
	  .height = height,
	  .width = width,
	  .data = data
	};
}

void set_pixel(struct image *image, size_t row, size_t column, struct pixel pixel) {
  image->data[image->width * row + column] = pixel;
}

struct pixel get_pixel(struct image *image, size_t row, size_t column) {
  return image->data[image->width * row + column];
}

void free_image(struct image *image) {
	free(image->data);
}

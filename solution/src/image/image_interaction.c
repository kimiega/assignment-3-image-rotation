#include "image_interaction.h"

void rotate(struct image *img) {
    struct image rotated_img = init_image(img->height, img->width);
    for (size_t i = 0; i < img->height; i++)
        for (size_t j = 0; j < img->width; j++) {
            struct pixel pixel = get_pixel(img, i, j);
            set_pixel(&rotated_img, j, img->height - i - 1, pixel);
        }
    free_image(img);
    *img = rotated_img;
}

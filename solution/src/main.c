#include "file_io.h"
#include "from_bmp.h"
#include "image.h"
#include "image_interaction.h"
#include "print_messages.h"
#include "to_bmp.h"

int main(int argc, char **argv) {
	if (argc != 3){
        print_error(INCORRECT_ARGS_MESSAGE);
        return 1;
    }
    FILE *source_file = NULL;
    FILE *transformed_file = NULL;
    char *source_filename = argv[1];
    char *transformed_filename = argv[2];
    struct image img;

    enum open_status open_status_source_file = open_file(&source_file, source_filename, "rb");
    if (open_status_source_file != OPEN_OK ) {
         print_error(OPEN_STATUS_MESSAGES[open_status_source_file]);
         return open_status_source_file;
     }
     enum read_status read_status_source_img = from_bmp(source_file, &img);
     if (read_status_source_img != READ_OK){
         print_error(READ_STATUS_MESSAGES[read_status_source_img]);
         free_image(&img);
        return read_status_source_img;
    }
     enum close_status close_status_source_file = close_file(source_file);
	if (close_status_source_file != CLOSE_OK) {
        print_error(CLOSE_STATUS_MESSAGES[close_status_source_file]);
        free_image(&img);
        return close_status_source_file;
    }

	rotate(&img);

    enum open_status open_status_transformed_file = open_file(&transformed_file, transformed_filename, "wb");
	if (open_status_transformed_file != OPEN_OK){
        print_error(OPEN_STATUS_MESSAGES[open_status_transformed_file]);
        free_image(&img);
        return open_status_transformed_file;
    };
    enum write_status write_status_transformed_file = to_bmp(transformed_file, &img);
	if (write_status_transformed_file != WRITE_OK){
        print_error(WRITE_STATUS_MESSAGES[write_status_transformed_file]);
        free_image(&img);
        return write_status_transformed_file;
    };
    enum close_status close_status_transformed_file = close_file(transformed_file);
	if (close_status_transformed_file != CLOSE_OK){
        print_error(CLOSE_STATUS_MESSAGES[close_status_transformed_file]);
        free_image(&img);
        return close_status_transformed_file;
    };
    free_image(&img);
    print_message(ALL_IS_GOOD_MESSAGE);
    return 0;
}

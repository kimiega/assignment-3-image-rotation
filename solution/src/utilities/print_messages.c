#include "print_messages.h"
#include <stdio.h>

void print_message(const char *message){
    fprintf(stdin,"%s\n", message);
}
void print_error(const char *message){
    fprintf(stderr,"%s\n", message);
}

#include "bmp_padding.h"
#include "image.h"

uint8_t get_padding(size_t width) {
  uint8_t padding = width * sizeof(struct pixel) % 4;
  if (padding)
    padding = 4 - padding;
  return padding;
}

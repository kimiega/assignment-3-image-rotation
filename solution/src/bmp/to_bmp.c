#include "to_bmp.h"
#include "bmp_header.h"
#include "bmp_padding.h"
#include "bmp_writter.h"

enum write_status to_bmp(FILE *out, struct image* img) {
  struct bmp_header header = create_bmp_header(img->width, img->height);
  enum write_status header_status = write_bmp_header(out, &header);
  return header_status == WRITE_OK ? write_bmp_pixels(out, img) : header_status;
}

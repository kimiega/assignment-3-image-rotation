#include "bmp_reader.h"
#include "bmp_padding.h"

#define ONE_ELEMENT 1

enum read_status read_header(FILE *in, struct bmp_header *header) {
    size_t result = fread(header, sizeof(struct bmp_header), ONE_ELEMENT, in);

    return result == ONE_ELEMENT ? READ_OK : READ_INVALID_HEADER;
}

enum read_status read_pixel(FILE *in, struct pixel *pixel) {
    return fread(pixel, sizeof(struct pixel), ONE_ELEMENT, in) == ONE_ELEMENT ?
    READ_OK : READ_INVALID_BITS;
}

enum read_status read_pixels(FILE *in, struct image* img) {
    size_t result;
    struct pixel *data = img->data;
    uint8_t padding = get_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++){
            enum read_status res = read_pixel(in, data + i * img->width + j);
            if (res != READ_OK)
                return READ_INVALID_SIGNATURE;
        }
        result = fseek(in, padding, SEEK_CUR);
        if (result)
            return READ_INVALID_BITS;
    }

    return READ_OK;
}

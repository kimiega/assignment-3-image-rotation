#include "bmp_header.h"
#include "bmp_padding.h"
#include "image.h"

#define TYPE 0x4d42
#define RESERVED 0
#define HEADER_SIZE 40
#define PLANES 1
#define BIT_PER_COLOR 24
#define COMPRESSION 0
#define RAND_CONST 1234
#define COLOR_USED 0
#define COLOR_IMPORTANT 0
#define ONE_ELEMENT 1

struct bmp_header create_bmp_header(size_t width, size_t height) {
    uint32_t header_size = sizeof(struct bmp_header);
    uint32_t image_size = sizeof(struct pixel) * width * height + get_padding(width) * height;
    return (struct bmp_header) {
            .bfType = TYPE,
            .bfileSize = header_size + image_size,
            .bfReserved = RESERVED,
            .bOffBits = header_size,
            .biSize = HEADER_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = PLANES,
            .biBitCount = BIT_PER_COLOR,
            .biCompression = COMPRESSION,
            .biSizeImage = image_size,
            .biXPelsPerMeter = RAND_CONST,
            .biYPelsPerMeter = RAND_CONST,
            .biClrUsed = COLOR_USED,
            .biClrImportant = COLOR_IMPORTANT
    };
}

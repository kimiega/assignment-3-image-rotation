#include "bmp_writter.h"
#include "bmp_padding.h"

#define ONE_ELEMENT 1

enum write_status write_bmp_header(FILE *out, struct bmp_header *header) {
    size_t result = fwrite(header, sizeof(struct bmp_header), ONE_ELEMENT, out);

    return result == ONE_ELEMENT ? WRITE_OK : WRITE_HEADER_ERROR;
}

enum write_status write_bmp_pixel(FILE *out, struct pixel *pixel) {
    return (fwrite(pixel, sizeof(struct pixel), ONE_ELEMENT, out) == ONE_ELEMENT) ?
            WRITE_OK : WRITE_PIXEL_ERROR;
}

enum write_status write_bmp_pixels(FILE *out, struct image *img) {
    size_t width = img->width;
    struct pixel* pointer = img->data;
    uint8_t padding = get_padding(width);
    uint32_t padding_zero = 0;
    size_t result;

    for (size_t i = 0; i < img->height; i++) {
        for (size_t j =0; j < img->width; j++) {
            enum write_status res = write_bmp_pixel(out, pointer + width * i + j);
            if ( res != WRITE_OK)
                return res;
        }
        result = fwrite(&padding_zero, 1, padding, out);
        if (result != padding)
            return WRITE_BIT_ERROR;
    }
    return WRITE_OK;
}

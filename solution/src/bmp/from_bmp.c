#include "from_bmp.h"
#include "bmp_header.h"
#include "bmp_reader.h"

enum read_status from_bmp( FILE *in, struct image* img ) {
	struct bmp_header bmp_header = {0};
	enum read_status header_read_status = read_header(in, &bmp_header);

	if (header_read_status != READ_OK)
		return header_read_status;

	*img = init_image(bmp_header.biWidth, bmp_header.biHeight);

	return read_pixels(in, img);
}

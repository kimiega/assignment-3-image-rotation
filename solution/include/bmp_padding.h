#ifndef BMP_PADDING_H
#define BMP_PADDING_H
#include <stdint.h>
#include <stdio.h>

uint8_t get_padding(size_t width);

#endif

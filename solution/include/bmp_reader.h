#ifndef BMP_READER_H
#define BMP_READER_H

#include "bmp_header.h"
#include "image.h"
#include "status_codes.h"

enum read_status read_header(FILE *in, struct bmp_header *header);
enum read_status read_pixels(FILE* in, struct image* img);

#endif

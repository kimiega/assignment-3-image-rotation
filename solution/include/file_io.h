#ifndef FILE_IO_H
#define FILE_IO_H
#include "status_codes.h"
#include <stdio.h>

enum open_status open_file(FILE **file, const char *filename, const char *mode);

enum close_status close_file(FILE *file);

#endif

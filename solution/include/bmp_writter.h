#ifndef BMP_WRITTER_H
#define BMP_WRITTER_H

#include "bmp_header.h"
#include "image.h"
#include "status_codes.h"

enum write_status write_bmp_header(FILE *out, struct bmp_header *header);
enum write_status write_bmp_pixels(FILE *out, struct image *img);

#endif

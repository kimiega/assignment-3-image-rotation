#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct pixel {
  uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
  size_t width, height;
  struct pixel *data;
};

struct image init_image(size_t width, size_t height);

void set_pixel(struct image *image, size_t row, size_t column, struct pixel pixel);

struct pixel get_pixel(struct image *image, size_t row, size_t column);

void free_image(struct image *image);

#endif

#ifndef STATUS_CODES_H
#define STATUS_CODES_H

extern const char *INCORRECT_ARGS_MESSAGE;
extern const char *ALL_IS_GOOD_MESSAGE;
extern const char *const READ_STATUS_MESSAGES[];
extern const char *const WRITE_STATUS_MESSAGES[];
extern const char *const OPEN_STATUS_MESSAGES[];
extern const char *const CLOSE_STATUS_MESSAGES[];

enum read_status {
  READ_OK = 0,
  READ_INVALID_SIGNATURE = 100,
  READ_INVALID_BITS = 101,
  READ_INVALID_HEADER = 102
  };
  
enum write_status {
  WRITE_OK = 0,
  WRITE_ERROR = 200,
  WRITE_HEADER_ERROR = 201,
  WRITE_PIXEL_ERROR = 202,
  WRITE_BIT_ERROR = 203
};

enum open_status {
	OPEN_OK = 0,
	OPEN_ERROR = 300
};

enum close_status {
	CLOSE_OK = 0,
	CLOSE_ERROR = 400
};

#endif

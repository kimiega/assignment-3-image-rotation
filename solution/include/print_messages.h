#ifndef PRINT_MESSAGES_H
#define PRINT_MESSAGES_H

void print_message(const char *message);
void print_error(const char *message);

#endif

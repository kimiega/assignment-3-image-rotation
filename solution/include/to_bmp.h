#ifndef TO_BMP_H
#define TO_BMP_H
#include "image.h"
#include "status_codes.h"

enum write_status to_bmp(FILE *out, struct image *img);

#endif

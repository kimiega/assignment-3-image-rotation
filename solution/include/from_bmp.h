#ifndef FROM_BMP_H
#define FROM_BMP_H
#include "image.h"
#include "status_codes.h"

enum read_status from_bmp(FILE *in, struct image *img);

#endif
